package Exercicis_de_Map;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import Varies.Cadena;
public class Waypoint {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
        ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
        return comprovacioRendimentTmp;
    }

    public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear, ComprovacioRendiment comprovacioRendimentTmp) {
        long tempsInicial;
        long tempsFinal;
        long tempsEnNanosegons;
        long tempsEnMilisegons;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        int[] coordenadesTmp = new int[] {0,0,0};

        tempsInicial = System.nanoTime();
        for (int i = 0; i < numObjACrear; i++) {
            comprovacioRendimentTmp.llistaArrayList.add(new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        }
        tempsFinal = System.nanoTime();
        tempsEnNanosegons = tempsFinal - tempsInicial;
        //tempsEnMilisegons = TimeUnit.MILLISECONDS.convert(tempsEnNanosegons, TimeUnit.NANOSECONDS);
        tempsEnMilisegons = Duration.ofNanos(tempsEnNanosegons).toMillis();
        System.out.println("Temps per a insertar " + numObjACrear + " waypoints en l'ArrayList: " + tempsEnMilisegons);

        tempsInicial = System.nanoTime();
        for (int i = 0; i < numObjACrear; i++) {
            comprovacioRendimentTmp.llistaLinkedList.add(new Waypoint_Dades(0, "Òrbita de la Terra", new int[] {0,0,0}, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        }
        tempsFinal = System.nanoTime();
        tempsEnNanosegons = tempsFinal - tempsInicial;
        //tempsEnMilisegons = TimeUnit.MILLISECONDS.convert(tempsEnNanosegons, TimeUnit.NANOSECONDS);
        tempsEnMilisegons = Duration.ofNanos(tempsEnNanosegons).toMillis();
        System.out.println("Temps per a insertar " + numObjACrear + " waypoints en el LinkedList: " + tempsEnMilisegons);
        return comprovacioRendimentTmp;
    }

    public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {
        long tempsInicial;
        long tempsFinal;
        int meitatLlista;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        int[] coordenadesTmp = new int[] {0,0,0};

        meitatLlista = comprovacioRendimentTmp.llistaArrayList.size()/2;
        System.out.print("llistaArrayList.size(): " + comprovacioRendimentTmp.llistaArrayList.size() + ", meitatLLista: " + meitatLlista + "\n");

        tempsInicial = System.nanoTime();
        comprovacioRendimentTmp.llistaArrayList.add(0, new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        tempsFinal = System.nanoTime();
        System.out.print("Temps per a insertar 1 waypoint en la 1a posició de l'ArrayList: ");
        System.out.println(tempsFinal - tempsInicial);

        tempsInicial = System.nanoTime();
        comprovacioRendimentTmp.llistaLinkedList.add(0, new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        tempsFinal = System.nanoTime();
        System.out.print("Temps per a insertar 1 waypoint en la 1a posició del LinkedList: ");
        System.out.println(tempsFinal - tempsInicial);
        System.out.println("------------");

        tempsInicial = System.nanoTime();
        comprovacioRendimentTmp.llistaArrayList.add(meitatLlista, new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        tempsFinal = System.nanoTime();
        System.out.print("Temps per a insertar 1 waypoint al mig (pos. " + meitatLlista + ") de l'ArrayList: ");
        System.out.println(tempsFinal - tempsInicial);

        tempsInicial = System.nanoTime();
        comprovacioRendimentTmp.llistaLinkedList.add(meitatLlista, new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        tempsFinal = System.nanoTime();
        System.out.print("Temps per a insertar 1 waypoint al mig (pos. " + meitatLlista + ") del LinkedList: ");
        System.out.println(tempsFinal - tempsInicial);
        System.out.println("------------");

        tempsInicial = System.nanoTime();
        comprovacioRendimentTmp.llistaArrayList.add(comprovacioRendimentTmp.llistaArrayList.size(), new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        tempsFinal = System.nanoTime();
        System.out.print("Temps per a insertar 1 waypoint al final de l'ArrayList: ");
        System.out.println(tempsFinal - tempsInicial);

        tempsInicial = System.nanoTime();
        comprovacioRendimentTmp.llistaLinkedList.add(comprovacioRendimentTmp.llistaLinkedList.size(), new Waypoint_Dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        tempsFinal = System.nanoTime();
        System.out.print("Temps per a insertar 1 waypoint al final del LinkedList: ");
        System.out.println(tempsFinal - tempsInicial);

        return comprovacioRendimentTmp;
    }

    public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
        int ultimaPosicio;
        Waypoint_Dades waypointTmp2;
        List<Integer> idsPerArrayList = new ArrayList<Integer>();

        System.out.println("---- APARTAT 1 ----");
        for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
            idsPerArrayList.add(i);
        }
        System.out.println("S'ha inicialitzat la llista idsPerArrayList amb " + idsPerArrayList.size() + " elements.");
        System.out.println("El 1r element té el valor: " + idsPerArrayList.get(0));
        ultimaPosicio = idsPerArrayList.size() - 1;
        System.out.println("L'últim element té el valor: " + idsPerArrayList.get(ultimaPosicio));

        System.out.println();
        System.out.println("---- APARTAT 2 ----");
        for (int i = 0; i < idsPerArrayList.size(); i++) {
            System.out.println("ABANS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get("+ i + ").getId(): " + comprovacioRendimentTmp.llistaArrayList.get(i).getId());
            comprovacioRendimentTmp.llistaArrayList.get(i).setId(idsPerArrayList.get(i).intValue());

            System.out.println("DESPRÉS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get("+ i + ").getId(): "+ comprovacioRendimentTmp.llistaArrayList.get(i).getId());
            System.out.println();
        }

        System.out.println();
        System.out.println("---- APARTAT 3.1 (bucle for) ----");
        for (Waypoint_Dades waypointTmp: comprovacioRendimentTmp.llistaArrayList) {
            System.out.println("ID = " + waypointTmp.getId() + ", nom = " + waypointTmp.getNom());
        }

        System.out.println();
        System.out.println("---- APARTAT 3.2 (Iterator) ----");
        Iterator<Waypoint_Dades> it = comprovacioRendimentTmp.llistaArrayList.iterator();
        while (it.hasNext()) {
            waypointTmp2 = it.next();
            System.out.println("ID = " + waypointTmp2.getId() + ", nom = " + waypointTmp2.getNom());
        }

        System.out.println();
        System.out.println("---- APARTAT 4 ----");
        System.out.println("Preparat per esborrar el contingut de llistaLinkedList que té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
        comprovacioRendimentTmp.llistaLinkedList.clear();
        System.out.println("Esborrada. Ara llistaLinkedList té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");
        comprovacioRendimentTmp.llistaLinkedList.addAll(comprovacioRendimentTmp.llistaArrayList);
        System.out.println("Copiats els elements de llistaArrayList en llistaLinkedList que ara té " + comprovacioRendimentTmp.llistaLinkedList.size() + " elements.");

        System.out.println();
        System.out.println("---- APARTAT 5 ----");
        int idTmp;
        for (Waypoint_Dades waypointTmp : comprovacioRendimentTmp.llistaArrayList) {
            idTmp = waypointTmp.getId();
            if (waypointTmp.getId() > 5) {
                waypointTmp.setNom("Òrbita de Mart");
                System.out.println("Modificat el waypoint amb id = " + waypointTmp.getId());
            }
        }

        System.out.println();
        System.out.println("---- APARTAT 5.1 (comprovació) ----");
        for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
            System.out.println("ID = " + comprovacioRendimentTmp.llistaArrayList.get(i).getId() + ", nom = " + comprovacioRendimentTmp.llistaArrayList.get(i).getNom());
        }

        System.out.println();
        System.out.println("---- APARTAT 5.1 (Iterator) ----");
        it = comprovacioRendimentTmp.llistaLinkedList.iterator();
        while (it.hasNext()) {
            waypointTmp2 = it.next();
            if (waypointTmp2.getId() < 5) {
                waypointTmp2.setNom("Punt Lagrange entre la Terra i la Lluna");
                System.out.println("Modificat el waypoint amb id = " + waypointTmp2.getId());
            }
        }

        System.out.println();
        System.out.println("---- APARTAT 5.2 (comprovació) ----");
        it = comprovacioRendimentTmp.llistaLinkedList.iterator();
        while (it.hasNext()) {
            waypointTmp2 = it.next();
            System.out.println("El waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(waypointTmp2.getId()).getId() + " té el nom = " + comprovacioRendimentTmp.llistaArrayList.get(waypointTmp2.getId()).getNom());
        }

        return comprovacioRendimentTmp;
    }

    public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
        Waypoint_Dades waypointTmp;
        System.out.println();
        System.out.println("---- APARTAT 1 (bucle for) ----");
//        int idTmp;
//        for (Waypoint_Dades waypointTmp: comprovacioRendimentTmp.llistaArrayList) {
//            idTmp = waypointTmp.getId();
//            if (waypointTmp.getId() < 6) {
//                comprovacioRendimentTmp.llistaArrayList.remove(idTmp);
//            }
//        }
        System.out.println("No es pot esborrar un waypoint amb el for perquè aquest no s'entera de que hi ha un element menys i peta");

        System.out.println();
        System.out.println("---- APARTAT 2 (Iterator) ----");
        Iterator<Waypoint_Dades> it = comprovacioRendimentTmp.llistaLinkedList.iterator();
        while (it.hasNext()) {
            waypointTmp = it.next();
            if (waypointTmp.getId() > 4) {
                System.out.println("Esborrat el waypoint amb id = " + waypointTmp.getId());
                it.remove();
            }
        }

        System.out.println();
        System.out.println("---- APARTAT 2 (comprovació) ----");
        it = comprovacioRendimentTmp.llistaLinkedList.iterator();
        while (it.hasNext()) {
            waypointTmp = it.next();
            System.out.println("El waypoint amb id = " + comprovacioRendimentTmp.llistaArrayList.get(waypointTmp.getId()).getId() + " té el nom = " + comprovacioRendimentTmp.llistaArrayList.get(waypointTmp.getId()).getNom());
        }

        System.out.println();
        System.out.println("---- APARTAT 3 (listIterator) ----");
        ListIterator<Waypoint_Dades> listIterator = comprovacioRendimentTmp.llistaLinkedList.listIterator();
        while (listIterator.hasNext()) {
            waypointTmp = listIterator.next();
            if (waypointTmp.getId() == 2) {
                System.out.println("Esborrat el waypoint amb id = " + waypointTmp.getId());
                listIterator.remove();
            }
        }

        System.out.println();
        System.out.println("---- APARTAT 3 (comprovació) ----");
        listIterator = comprovacioRendimentTmp.llistaLinkedList.listIterator(comprovacioRendimentTmp.llistaLinkedList.size()-1);
        while (listIterator.hasPrevious()) {
            waypointTmp = listIterator.previous();
            System.out.println("El waypoint amb id = " + waypointTmp.getId() + " té el nom = " + waypointTmp.getNom());
        }
        return comprovacioRendimentTmp;
    }

    public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
        Waypoint_Dades waypointTmp;
        Scanner sc = new Scanner(System.in);
        String nomNou;
        String coordenadesNoves = null;
        int posicio;
        int numParamLlegits;
        int[] coordenadesTmp;
        boolean coordenadesCorrectes;

        Iterator<Waypoint_Dades> it = comprovacioRendimentTmp.llistaArrayList.iterator();

        while (it.hasNext()) {
            waypointTmp = it.next();

            if (waypointTmp.getId() % 2 == 0) {
                System.out.println("------ Modificar el waypoint amb id = " + waypointTmp.getId() + " ------");

                System.out.println("Nom actual: " + waypointTmp.getNom());
                System.out.println("Nom nou: ");
                nomNou = sc.nextLine();
                waypointTmp.setNom(nomNou);

                coordenadesCorrectes = false;
                while (coordenadesCorrectes == false) {
                    numParamLlegits = 0;
                    while (numParamLlegits != 3) {
                        System.out.println("Coordenades actuals: " + waypointTmp.getCoordenades()[0] + " " + waypointTmp.getCoordenades()[1] +
                                " " + waypointTmp.getCoordenades()[2]);
                        System.out.print("Coordenades noves (format: 1 13 7): ");
                        coordenadesNoves = sc.nextLine();

                        numParamLlegits = coordenadesNoves.split(" ").length;
                        if (numParamLlegits != 3) {
                            System.out.println("ERROR: introduir 3 paràmentres separats per 1 espai en blanc. Has introduit " + numParamLlegits + " paràmetres.");
                        }
                    }

                    posicio = 0;
                    coordenadesTmp = new int[] {0,0,0};
                    for (String coordenada: coordenadesNoves.split(" ")) {
                        if (Cadena.stringIsInt(coordenada)) {
                            coordenadesTmp[posicio] = Integer.parseInt(coordenada);
                            posicio++;
                        } else {
                            System.out.println("ERROR: coordenada " + coordenada + " no vàlida.");
                        }
                    }

                    if (posicio == 3) {
                        waypointTmp.setCoordenades(coordenadesTmp);
                        coordenadesCorrectes = true;
                    }
                }

                System.out.println();
            }
        }

        return comprovacioRendimentTmp;
    }

    public static void visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
        ArrayList<Waypoint_Dades> llistaWaypointsOrdenats = new ArrayList<Waypoint_Dades>();

        llistaWaypointsOrdenats.addAll(comprovacioRendimentTmp.llistaArrayList);

        Collections.sort(llistaWaypointsOrdenats);

        for (Waypoint_Dades waypointTmp: llistaWaypointsOrdenats) {
            System.out.println(waypointTmp);
        }
    }

    public static void waypointsACertaDistanciaMaxDeLaTerra(ComprovacioRendiment comprovacioRendimentTmp) {
        Scanner sc = new Scanner(System.in);
        String distanciaALaTerraString;
        int distanciaALaTerraInt;
        ArrayList<Waypoint_Dades> llistaWaypointsOrdenats = new ArrayList<Waypoint_Dades>();
        int distancia = 0;

        System.out.println("Distància màxima de la Terra: ");
        distanciaALaTerraString = sc.nextLine();
        while (Cadena.stringIsInt(distanciaALaTerraString) == false) {
            System.out.println("ERROR: la distància màxima de la Terra " + distanciaALaTerraString + " no és correcta.");
            System.out.println("Distància màxima de la Terra: ");
            distanciaALaTerraString = sc.nextLine();
        }

        distanciaALaTerraInt = Integer.parseInt(distanciaALaTerraString);
        llistaWaypointsOrdenats.addAll(comprovacioRendimentTmp.llistaArrayList);
        Collections.sort(llistaWaypointsOrdenats);
        for (Waypoint_Dades waypointTmp: llistaWaypointsOrdenats) {
            distancia = (int)Math.pow(waypointTmp.getCoordenades()[0], 2) + (int)Math.pow(waypointTmp.getCoordenades()[1], 2) + (int)Math.pow(waypointTmp.getCoordenades()[2], 2);
            if (distancia <= distanciaALaTerraInt) {
                System.out.println(waypointTmp);
            } else {
                break;
            }
        }
    }
}
