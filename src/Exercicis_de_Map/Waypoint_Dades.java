package Exercicis_de_Map;

import java.text.Collator;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Locale;

public class Waypoint_Dades implements Comparable<Waypoint_Dades> {
    private int id;
    private String nom;
    private int[] coordenades;
    private boolean actiu;
    private LocalDateTime dataCreacio;
    private LocalDateTime dataAnulacio;
    private LocalDateTime dataModificacio;

    public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio, LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
        this.id = id;
        this.nom = nom;
        this.coordenades = coordenades;
        this.actiu = actiu;
        this.dataCreacio = dataCreacio;
        this.dataAnulacio = dataAnulacio;
        this.dataModificacio = dataModificacio;
    }

    @Override
    public String toString() {
        String cadenaAImprimir;
        int distancia = 0;

        distancia = (int)Math.pow(this.getCoordenades()[0], 2) + (int)Math.pow(this.getCoordenades()[1], 2) + (int)Math.pow(this.getCoordenades()[2], 2);

        cadenaAImprimir = "WAYPOINT " + id +":\n\tnom = "+ nom +
                "\n\tcoordenades(x, y, z) = (" + coordenades[0] + "," + coordenades[1] + "," + coordenades[2] + ") (distància = " +
                distancia +"\n\tactiu = "+ actiu;
        if (dataCreacio == null) {
            cadenaAImprimir += "\n\tdataCreacio = NULL";
        } else {
            cadenaAImprimir += "\n\tdataCreacio = " + dataCreacio.format(IKSRotarranConstants.formatter);
        }

        if (dataAnulacio == null) {
            cadenaAImprimir += "\n\tdataAnulacio = NULL";
        } else {
            cadenaAImprimir += "\n\tdataAnulacio = " + dataAnulacio.format(IKSRotarranConstants.formatter);
        }

        if (dataModificacio == null) {
            cadenaAImprimir += "\n\tdataModificacio = NULL";
        } else {
            cadenaAImprimir += "\n\tdataModificacio = " + dataModificacio.format(IKSRotarranConstants.formatter);
        }
        return cadenaAImprimir;
    }



    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[] getCoordenades() {
        return coordenades;
    }

    public void setCoordenades(int[] coordenades) {
        this.coordenades = coordenades;
    }

    @Override
    public int compareTo(Waypoint_Dades o) {
        int result = 0;
        int distanciaM1 = 0;
        int distanciaM2 = 0;
        if (Arrays.equals(this.getCoordenades(), o.getCoordenades())) {
            Collator tertiaryCollator = Collator.getInstance(new Locale("es"));
            tertiaryCollator.setStrength(Collator.TERTIARY);
            result = tertiaryCollator.compare(this.getNom(), o.getNom());
        } else {
            distanciaM1 = (int)Math.pow(this.getCoordenades()[0], 2) + (int)Math.pow(this.getCoordenades()[1], 2) + (int)Math.pow(this.getCoordenades()[2], 2);
            distanciaM2 = (int)Math.pow(o.getCoordenades()[0], 2) + (int)Math.pow(o.getCoordenades()[1], 2) + (int)Math.pow(o.getCoordenades()[2], 2);

            if (distanciaM1 < distanciaM2) {
                result = -1;
            } else if (distanciaM1 > distanciaM2) {
                result = 1;
            } else {
                result = 0;
            }
        }

        return result;
    }
}