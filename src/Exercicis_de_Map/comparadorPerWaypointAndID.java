package Exercicis_de_Map;

import java.util.Comparator;
import java.util.LinkedHashMap;

public class comparadorPerWaypointAndID implements Comparator {
	public LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes;
	
	
	

	public comparadorPerWaypointAndID(LinkedHashMap<Integer, Ruta_Dades> mapaLinkedDeRutes) {
		this.mapaLinkedDeRutes = mapaLinkedDeRutes;
	}

	@Override
	public int compare(Object o1, Object o2) {
		Comparable valorA = (Comparable) mapaLinkedDeRutes.get(o1);
        Comparable valorB = (Comparable) mapaLinkedDeRutes.get(o2);
        
        return valorA.compareTo(valorB);
	}

}
