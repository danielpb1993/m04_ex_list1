package Exercicis_de_Map;

import java.time.format.DateTimeFormatter;

public interface IKSRotarranConstants {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
}
