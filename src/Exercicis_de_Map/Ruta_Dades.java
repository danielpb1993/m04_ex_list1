package Exercicis_de_Map;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Ruta_Dades implements Comparable<Ruta_Dades>{
    private int id;
    private String nom;
    private ArrayList<Integer> waypoints;
    private boolean actiu;
    private LocalDateTime dataCreacio;
    private LocalDateTime dataAnulacio;
    private LocalDateTime dataModificacio;


    public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio, LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
        this.id = id;
        this.nom = nom;
        this.waypoints = waypoints;
        this.actiu = actiu;
        this.dataCreacio = dataCreacio;
        this.dataAnulacio = dataAnulacio;
        this.dataModificacio = dataModificacio;
    }

    @Override
    public String toString() {
        return "Ruta_Dades{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", waypoints=" + waypoints +
                ", actiu=" + actiu +
                ", dataCreacio=" + dataCreacio +
                ", dataAnulacio=" + dataAnulacio +
                ", dataModificacio=" + dataModificacio +
                '}';
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public ArrayList<Integer> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(ArrayList<Integer> waypoints) {
        this.waypoints = waypoints;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Ruta_Dades o) {
        if (this.getWaypoints().equals(o.getWaypoints())) {
            return 0;
        } else {
            return o.getId() - this.getId();
        }
    }
}